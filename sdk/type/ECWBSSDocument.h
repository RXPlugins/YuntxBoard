//
//  ECWBSSDocument.h
//  WBSSiPhoneSDK
//
//  Created by jiazy on 16/6/20.
//  Copyright © 2016年 yuntongxun. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 文档类
 */
@interface ECWBSSDocument : NSObject

/**
 @brief 所属房间ID
 */
@property (nonatomic, assign) int roomId;

/**
 @brief 文档ID
 */
@property (nonatomic, assign) int documentId;

/**
 @brief 当前显示页
 */
@property (nonatomic, assign) int currentPage;

/**
 @brief 文档的页数
 */
@property (nonatomic, assign) int pageCount;

/**
 @brief 文档名称
 */
@property (nonatomic, copy) NSString* fileName;

/**
 @brief 文档路径(上传文档使用)
 */
@property (nonatomic, copy) NSString* filePath;

/**
 @brief fileType 1,原始文档 2,白板贴图（使用时用枚举）3,url网络路径
 */
@property (nonatomic, assign) int fileType;
@end
