#
#  Be sure to run `pod spec lint YuntxWBLib.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "YuntxWBLib"
  s.version      = "2.2.0.0"
  s.summary      = "容联云通讯白板SDK(如果看不到最新版本，请使用 pod repo update 命令更新一下本地pod仓库)"

  
  s.description  = <<-DESC
            容联云通讯白板SDK YuntxWBLib (如果看不到最新版本，请使用 pod repo update 命令更新一下本地pod仓库)
                   DESC

  s.homepage     = "https://www.yuntongxun.com/"
  s.license      = "MIT"
  s.author       = { "容联云通讯" => "ronglianssy@163.com" }
  s.ios.deployment_target = "8.0"
  s.source          = { :git => "https://gitlab.com/RXPlugins/YuntxBoard.git", :tag => "#{s.version}" }
  s.resources       = "sdk/ECWBSSBundle.bundle"
  s.source_files    = "sdk/*.h"
  s.vendored_library = 'sdk/*.a'
  s.framework    = 'OpenGLES' 
  # s.dependency "YuntxIMLib"

  s.subspec 'delegate' do |ss|
  ss.source_files = "sdk/delegate/*.h"
  end

  s.subspec 'type' do |ss|
  ss.source_files = "sdk/type/*.h"
  end

  s.subspec 'manager' do |ss|
  ss.source_files = "sdk/manager/*.h"
  end
  

end
